import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
    this.route('home');
    this.route('production');
    this.route('consomation');
    this.route('plan');
    this.route('defis');
    this.route('trajets');
});

export default Router;
